<?php

use App\Http\Controllers\CuriculumVitaeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//routing ke halaman view "welcome"
Route::get('/welcome', function () {
    return view('welcome');
});

//routing menampilkan return value "Hellow"
Route::get('/hello', function () {
    return "Hellow";
});

//routing ke halaman view belajar-blade
Route::get('/belajar-blade', function () {
    return view('belajar-blade');
});

//routing ke halaman view index dashboard
Route::get('/', function () {
    return view('dashboard.index');
});

//routing ke halaman view index data bootcamp
//tanpa controller
// Route::get('/bootcamp-data', function () {
//     return view('bootcamp-data.index');
// });
//dengan controller
Route::get('/bootcamp-data', [UserController::class, 'index']);
Route::get('/bootcamp-data/{id}', 'UserController@show');
Route::get('/bootcamp-data/edit/{id}', 'UserController@edit');
Route::post('/bootcamp-data/update', 'UserController@update');
Route::get('/bootcamp-data/delete/{id}', 'UserController@destroy');
// Route::delete('/bootcamp-data/delete/{id}', 'UserController@destroy');
// Route::resource('/bootcamp-data', UserController::class);

//routing ke halaman cv
// Route::get('/cv', [CuriculumVitaeController::class, 'index']);
Route::get('/cv', 'CuriculumVitaeController@index'); //using namespace on RouteServiceProvider

Route::get('/login', function () {
    return view('login.index');
});
Route::get('/register', function () {
    return view('register.index');
});
