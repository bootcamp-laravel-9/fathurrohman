<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            [
                'name' => 'Fatur',
                'univ' => 'UPN',
                'asal' => 'Yogyakarta'
            ],
            [
                'name' => 'Bagas',
                'univ' => 'UAJY',
                'asal' => 'Kalimantan'
            ],
            [
                'name' => 'Novianto',
                'univ' => 'UAJY',
                'asal' => 'Sumatera'
            ],
            [
                'name' => 'Nuel',
                'univ' => 'UKDW',
                'asal' => 'Yogyakarta'
            ],
            [
                'name' => 'Kiki',
                'univ' => 'UKDW',
                'asal' => 'Yogyakarta'
            ],
            [
                'name' => 'Edwin',
                'univ' => 'UKDW',
                'asal' => 'Yogyakarta'
            ],
            [
                'name' => 'Ryzal',
                'univ' => 'UPN',
                'asal' => 'Sumedang'
            ],
            [
                'name' => 'Christo',
                'univ' => 'UAJY',
                'asal' => 'Tegal'
            ],
            [
                'name' => 'Samuel',
                'univ' => 'UKDW',
                'asal' => 'Purworejo'
            ],
            [
                'name' => 'Evan',
                'univ' => 'UAJY',
                'asal' => 'Solo'
            ]
        ];

        DB::beginTransaction();
        foreach ($members as $member) {
            Member::firstOrCreate($member);
        }
        DB::commit();
    }
}
