@extends('layout.main')
@section('menu-cv', 'active')
@section('header-title', 'Curiculum Vitae')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">

                <div class="card">
                    <div class="card-body">
                        <h2>About me</h2>
                        <div class="row my-auto">
                            <div class="col-md-3 my-auto">
                                <img src="{{ asset('./assets/images/cv-photo.png') }}" class="img-fluid" alt="photo"
                                    style="height: 200px; width:100%;">
                            </div>
                            <div class="col-md-9">
                                <p>Name : John Doe</p>
                                <p>University : Chicago Master University</p>
                                <p>Working Experience : 23 years</p>
                                <p>Age : 24</p>
                                <p>Experienced web developer passionate about crafting dynamic and user-friendly digital
                                    experiences. Proficient in a variety of programming languages and frameworks, with a
                                    keen eye for design and usability. Dedicated to staying current with emerging
                                    technologies and trends in the ever-evolving landscape of web development. Committed to
                                    delivering high-quality, scalable solutions that meet client objectives and exceed user
                                    expectations. Let's build something incredible together.</p>
                                <p> With a solid foundation in front-end and back-end development, I specialize in bringing
                                    ideas to life through clean, efficient code and intuitive interfaces. My expertise spans
                                    HTML, CSS, JavaScript, and popular frameworks such as React, Angular, and Vue.js,
                                    enabling me to create responsive, interactive websites and web applications tailored to
                                    specific project requirements. </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h2>Work</h2>
                        <div class="work-experience">
                            <small class="date">2024-2020</small>
                            <h3 class="h5 date-title">Frontend developer - <a href="http://en.orson.io"
                                    title="Create professionnal website">Orson.io</a></h3>
                            <p>Developed dynamic and user-friendly web interfaces using HTML5, CSS3, and JavaScript,
                                ensuring cross-browser compatibility and responsiveness, Collaborated closely with design
                                and back-end teams to translate wireframes and mockups into interactive web experiences,
                                Implemented front-end frameworks such as React, Angular, or Vue.js to create robust and
                                scalable web applications.</p>
                        </div>

                        <div class="work-experience">
                            <small class="date">2020-2015</small>
                            <h3 class="h5 date-title">Wordpress developer - <a href="http://mashup-template.com"
                                    title="">Machina Tech</a></h3>
                            <p>Developed custom WordPress themes and plugins from scratch, ensuring adherence to coding
                                standards and best practices, Customized existing WordPress themes to meet client
                                specifications, including design modifications and feature enhancements, Collaborated with
                                design and marketing teams to create visually appealing and user-friendly
                                website layouts.</p>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h2>Education</h2>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="education-experience">
                                    <small class="date">2017-2015</small>
                                    <h3 class="h5 date-title">Computer Master</h3>
                                    <p>Chicago University</p>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="education-experience">
                                    <small class="date">2015-2012</small>
                                    <h3 class="h5 date-title">Mathematics</h3>
                                    <p>Ecole 87</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="education-experience">
                                    <small class="date">2012-2011</small>
                                    <h3 class="h5 date-title">Motion Design Course</h3>
                                    <p>Pascal’s Lee Studio</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h2>Skill</h2>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="./assets/images/img-02.jpg" class="img-responsive" alt="">
                                <h3 class="h5">React JS</h3>
                                <p>Profesional</p>
                            </div>
                            <div class="col-md-4">
                                <img src="./assets/images/img-03.jpg" class="img-responsive" alt="">
                                <h3 class="h5">ASP .Net</h3>
                                <p>Intermediate</p>
                            </div>
                            <div class="col-md-4">
                                <img src="./assets/images/img-04.jpg" class="img-responsive" alt="">
                                <h3 class="h5">Phyton</h3>
                                <p>Amateur</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h2>Training & Certification</h2>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="education-experience">
                                    <small class="date">2022</small>
                                    <h3 class="h5 date-title">React JS Mastery</h3>
                                    <p>Chicago University</p>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="education-experience">
                                    <small class="date">2023</small>
                                    <h3 class="h5 date-title">Microsoft .Net Certification</h3>
                                    <p>Microsoft</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="education-experience">
                                    <small class="date">2024</small>
                                    <h3 class="h5 date-title">Phyton Intro</h3>
                                    <p>Udemy</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
