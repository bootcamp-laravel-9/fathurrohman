@extends('layout.main')
@section('menu-data', 'active')
@section('header-title', 'Data Peserta Bootcamp')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Bordered Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>Asal Kampus</th>
                            <th>Asal Daerah</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($userData as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row['name'] }}</td>
                                {{-- <td>{{ $row['univ'] }}</td> --}}
                                {{-- @if ($row['univ'] == 'UPN')
                                    <td class="text-danger">{{ $row['univ'] }}</td>
                                @elseif ($row['univ'] == 'UAJY')
                                    <td class="text-success">{{ $row['univ'] }}</td>
                                @elseif ($row['univ'] == 'UKDW')
                                    <td class="text-primary">{{ $row['univ'] }}</td>
                                @else
                                    <td>{{ $row['univ'] }}</td>
                                @endif --}}
                                <td
                                    class="{{ $row['univ'] === 'UPN' ? 'text-danger' : ($row['univ'] === 'UAJY' ? 'text-success' : ($row['univ'] === 'UKDW' ? 'text-primary' : '')) }}">
                                    {{ $row['univ'] }}</td>

                                <td>{{ $row['asal'] }}</td>
                                <td><a href={{ url('/bootcamp-data/' . $row['id']) }} class="btn btn-primary mr-2">Detail</a>
                                    <a href={{ url('/bootcamp-data/edit/' . $row['id']) }}
                                        class="btn btn-success mr-2">Edit</a>
                                    <a href={{ url('/bootcamp-data/delete/' . $row['id']) }} class="btn btn-danger mr-2"><i
                                            class="fas fa-trash"></i></a>
                                    {{-- <form action="{{ url('/bootcamp-data/delete/' . $row['id']) }}" method="post"
                                        class="d-inline">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger"
                                            onclick="return confirm('Yakin hapus {{ $row['name'] }}?')"><i
                                                class="fas fa-trash"></i></button>
                                    </form> --}}
                                </td>
                            </tr>
                        @endforeach
                        {{-- <tr>
                            <td>1.</td>
                            <td>Fathurrohman</td>
                            <td>UPN "Veteran" Yogyakarta</td>
                            <td>Yogyakarta</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Novianto</td>
                            <td>UAJY</td>
                            <td>Sumatera Selatan</td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>Bagas</td>
                            <td>UAJY</td>
                            <td>Kalimantan Tengah</td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Imanuel</td>
                            <td>UKDW</td>
                            <td>Yogyakarta</td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td>Kiki</td>
                            <td>UKDW</td>
                            <td>Yogyakarta</td>
                        </tr>
                        <tr>
                            <td>6.</td>
                            <td>Edwin</td>
                            <td>UKDW</td>
                            <td>Yogyakarta</td>
                        </tr>
                        <tr>
                            <td>7.</td>
                            <td>Ryzal</td>
                            <td>UPN "Veteran" Yogyakarta</td>
                            <td>Sumedang</td>
                        </tr>
                        <tr>
                            <td>8.</td>
                            <td>Samuel</td>
                            <td>UKDW</td>
                            <td>Purworejo</td>
                        </tr>
                        <tr>
                            <td>9.</td>
                            <td>Evan</td>
                            <td>UAJY</td>
                            <td>Solo</td>
                        </tr>
                        <tr>
                            <td>10.</td>
                            <td>Christo</td>
                            <td>UAJY</td>
                            <td>Tegal</td>
                        </tr> --}}

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            {{-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
            </div> --}}
        </div>
    </div>
@endsection
