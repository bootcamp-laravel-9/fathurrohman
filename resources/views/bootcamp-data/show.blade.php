@extends('layout.main')
@section('menu-data', 'active')
@section('header-title', 'Detail Peserta')

@section('content')
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Detail - {{ $userDetail->name }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if ($action == 'edit')
                <form action="{{ url('/bootcamp-data/update') }}" method="post">
                @else
                    <form>
            @endif
            @csrf
            <input type hidden name="id" value="{{ $userDetail->id }}">
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $userDetail->name }}"
                        {{ $action == 'show' ? 'readonly' : '' }}>
                </div>
                <div class="form-group">
                    <label for="univ">Universitas</label>
                    <input type="text" class="form-control" id="univ" name="univ" value="{{ $userDetail->univ }}"
                        {{ $action == 'show' ? 'readonly' : '' }}>
                </div>
                <div class="form-group">
                    <label for="asal">Asal Daerah</label>
                    <input type="text" class="form-control" id="asal" name="asal" value="{{ $userDetail->asal }}"
                        {{ $action == 'show' ? 'readonly' : '' }}>
                </div>
                @if ($action == 'edit')
                    <button type="submit" class="btn btn-primary">Submit</button>
                @endif
                </form>
            </div>
            <!-- /.card -->
        </div>
    @endsection
