@extends('layout.main')
@section('menu-data', 'active')
@section('header-title', 'Ubah Peserta')

@section('content')
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Detail - {{ $userDetail->name }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" value="{{ $userDetail->name }}">
                    </div>
                    <div class="form-group">
                        <label for="univ">Universitas</label>
                        <input type="text" class="form-control" id="univ" value="{{ $userDetail->univ }}">
                    </div>
                    <div class="form-group">
                        <label for="asal">Asal Daerah</label>
                        <input type="text" class="form-control" id="asal" value="{{ $userDetail->asal }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
@endsection
