<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = [
        //     ['nama' => 'Fatur', 'asal' => 'Yogyakarta', 'univ' => 'UPN'],
        //     ['nama' => 'Bagas', 'asal' => 'Kalimantan', 'univ' => 'UAJY'],
        //     ['nama' => 'Alwi', 'asal' => 'Sumatera Selatan', 'univ' => 'UAJY'],
        //     ['nama' => 'Nuel', 'asal' => 'Yogyakarta', 'univ' => 'UKDW'],
        //     ['nama' => 'Kiki', 'asal' => 'Yogyakarta', 'univ' => 'UKDW'],
        //     ['nama' => 'Edwin', 'asal' => 'Yogyakarta', 'univ' => 'UKDW'],
        //     ['nama' => 'Ryzal', 'asal' => 'Sumedang', 'univ' => 'UPN'],
        //     ['nama' => 'Christo', 'asal' => 'Tegal', 'univ' => 'UAJY'],
        //     ['nama' => 'Samuel', 'asal' => 'Purworejo', 'univ' => 'UKDW'],
        //     ['nama' => 'Evan', 'asal' => 'Solo', 'univ' => 'UAJY'],
        // ];

        $data = Member::all();

        return view('bootcamp-data.index', ['userData' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $detail = Member::where('id', $id)->first();
        $detail = Member::find($id);
        // dd($detail);
        return view('bootcamp-data.show', ['userDetail' => $detail, 'action' => 'show']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Member::find($id);
        return view('bootcamp-data.show', ['userDetail' => $detail, 'action' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->id);
        $updatedData = Member::find($request->id);

        $updatedData->id = $request->id;
        $updatedData->name = $request->name;
        $updatedData->univ = $request->univ;
        $updatedData->asal = $request->asal;
        // dd($updatedData);

        if ($updatedData->save()) {
            return redirect('/bootcamp-data');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Member::destroy($id);
        return redirect('bootcamp-data')->with('success', 'Berhasil dihapus!');
    }
}
